import React, { Component } from 'react'

class RobotForm extends Component {
    constructor(props){
        super(props)
        this.state = {
            id:'',
            type:'',
            name:'',
            mass:''
        }
        this.handleChange = (evt) =>{
            this.setState({
                [evt.target.name] : evt.target.value
            })
        }
        this.handleClick = () => {
            this.props.onAdd({
                id: this.state.id,
                type: this.state.type,
                name:this.state.name,
                mass:this.state.mass,
                
            })
           
        }
    }
    render(){
        return <div>
            <input type="text" placeholder="name" id="name" name="name" onChange={this.handleChange}/>
            <input type="text" placeholder="type" id="type" name="type" onChange={this.handleChange}/>
            <input type="text" placeholder="mass" id="mass" name="mass" onChange={this.handleChange}/>
            <input type="button" value="add" onClick={this.handleClick}/>
        </div>
    }
}
export default RobotForm